package com.example.mapper;

import com.example.dto.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper {


    @Insert("insert into user(username,password) values(#{username},#{password})")
    public void register(User user);
}
